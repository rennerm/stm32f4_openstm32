# OpenSTM32 for STM32F4 V1.0
Copyright (C) 2016  Matthias Renner

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## 1. Introduction

This project's goal is have a working environment for STM32F4 on Linux (and Windows) with Eclipse as opensource IDE.

The project is depending on OpenSTM32 plug-in and Eclipse (for installation instructions go [here](http://www.openstm32.org/Installing+System+Workbench+for+STM32+from+Eclipse?structure=Documentation)).

## 2. Instructions

1. Set up Eclipse with OpenSTM32 plug-in ([link](http://www.openstm32.org/Installing+System+Workbench+for+STM32+from+Eclipse?structure=Documentation))
2. go to your Eclipse workspace and clone this project:

    * for project with all its subprojects:

            git clone --recursive git@bitbucket.org:rennerm/stm32f4_openstm32.git

    * only project with specific subproject:

            git clone git@bitbucket.org:rennerm/stm32f4_openstm32.git
            cd stm32f4_openstm32
            git submodule update --init -- Projects/specific_subproject


3. in Eclipse select: *File --> Import --> General --> Existing Projects into Workspace*
4. Browse to *stm32f4_openstm32/Libraries/stm32f4discovery_stdperiph_lib* and import it
5. repeat step 3 and 4 for every specific subproject you need *stm32f4_openstm32/Projects/specific_subproject*
6. Build it
